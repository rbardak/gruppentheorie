# COMPILING

If you already have a *full* installation you can simply change to the
source directory and run

    latexmk

For detailed instructions see below.

## Debian-based GNU/Linux Distributions

Open a terminal and type

    sudo apt-get install texlive-full

This will install the full TeXlive distribution.  You can then compile
the source simply using

    latexmk

## Remarks

The official release version of the script uses the Lucida fonts in
terms of the lucimatx package.  All spacing was optimised for that
font.  If you don't use the Lucida font, expect overfull and underfull
boxes.

Building in dvi-mode will fail, due to the microtype package.

Building was tested with TeXlive 2014.