Um das verständlicher zu machen, betrachten wir zwei einfache
Beispiele.

Sei $\mathcal H = (5\mathbb Z,+) \defineeq \{ 0,\pm 5,\pm 10,\ldots
\}$, welches eine Untergruppe zu $(\mathbb Z,+)$ ist.  Eine
Nebenklasse ist zum Beispiel
\begin{align*}
  0(5\mathbb Z) &= 0 + 5\mathbb Z \;, \\
  2(5\mathbb Z) &= 2 + 5\mathbb Z = \{ \ldots,-8,-5,-3,2,7,12,\ldots \} \;.
\end{align*}
Es gilt die Zerlegung
\begin{equation*}
  \mathbb Z = (0 + 5\mathbb Z) \cup (1 + 5\mathbb Z) \cup
  (2 + 5\mathbb Z) \cup (3 + 5\mathbb Z) \cup (4 + 5\mathbb Z) \;.
\end{equation*}
Die Faktormenge ist
\begin{equation*}
  \mathbb Z/5\mathbb Z = \{ 5\mathbb Z,1 + 5\mathbb Z,
  2 + 5\mathbb Z,3 + 5\mathbb Z,4 + 5\mathbb Z \} \;.
\end{equation*}

Es gilt $\mathrm{SO}(2) < \mathrm{SO}(3)$, wobei $\mathrm{SO}(2)$ die
Drehungen um die $\hat{\bm e}_3$-Achse enthalten soll.  Die
Nebenklasse sei $g\mathrm{SO}(2)$, wobei $g \in \mathrm{SO}(3)$ und
enthält alle Drehungen um die Achse $g\hat{\bm e}_3$.  Die Faktormenge
$\mathrm{SO}(3)/\mathrm{SO}(2)$ enthält alle Drehachsen und Drehungen
um diese.  Sie ist isomorph zur $2$-Sphäre $S^2$.

\begin{theorem}
  \label{thm:2.4}
  Ist $\mathcal H$ endlich, so enthält jede Rechtsnebenklasse von
  $\mathcal H$ genauso viele Elemente wie $\mathcal H$ selbst.
\end{theorem}

\begin{proof}
  Zu zeigen ist: Die Abbildung $\psi\colon\mathcal H \to g\mathcal H,
  h \mapsto gh$ ist bijektiv.

  $g\mathcal H$ ist erst durch die Abbildung $\psi$ definiert, also
  per Definition surjektiv.  $\psi$ ist auch injektiv, denn aus
  \begin{equation*}
    \psi(k) = \psi(\ell) \iff gk = g\ell
  \end{equation*}
  folgt durch Multiplikation mit $g^{-1}$ von links $k = \ell$.
\end{proof}

Das heißt, jede endliche Gruppe $\mathcal G$ lässt sich disjunkt in
gleichmächtige Teilmengen $g\mathcal H = \{ gh, h\in\mathcal H,
\mathcal H<\mathcal G, g\in\mathcal G \}$ bzw.\ $\mathcal H g = \{ hg,
h\in\mathcal H, \mathcal H<\mathcal G, g\in\mathcal G \}$ zerlegen.

\begin{definition}
  \label{def:2.14}
  Ist $\mathcal G$ eine endliche Gruppe, so bezeichnet man die Anzahl
  der Elemente von $\mathcal G$ als \acct{Ordnung} von $\mathcal G$,
  geschrieben $\operatorname{ord}\mathcal G$ oder $\lvert\mathcal
  G\rvert$.
\end{definition}

\begin{definition}
  \label{def:2.15}
  Ist $\mathcal G$ eine endliche Gruppe und $\mathcal H<\mathcal G$
  Untergruppe, so heißt die Anzahl der Rechtsnebenklassen bezüglich
  $\mathcal H$ der Index von $\mathcal H$ in $\mathcal G$, geschrieben
  $[\mathcal G:\mathcal H]$ oder $\operatorname{ind}\mathcal H$.
\end{definition}

\begin{theorem}
  \label{thm:2.5}
  (ohne Beweis) Ist $\mathcal G$ eine endliche Gruppe und $\mathcal
  H<\mathcal G$, so ist $\lvert\mathcal H\rvert$ Teiler von
  $\lvert\mathcal G\rvert$ und es gilt
  \begin{equation*}
    \lvert\mathcal G\rvert = [\mathcal G:\mathcal H]
    \cdot \lvert\mathcal H\rvert
  \end{equation*}
\end{theorem}

\begin{notice}[Folgerung:]
  Ist $\lvert\mathcal G\rvert$ Primzahl, so besitzt $\mathcal G$ keine
  echten (eigentlichen) Untergruppen.
\end{notice}

\section{Orbits und Bahnen}

\subsection{Einführung}

\begin{definition}
  \label{def:2.16}
  Es wirke die Gruppe $\mathcal G$ auf die Menge $M$.  Man sagt, $x\in
  M$ ist $\mathcal G$-äquivalent zu $y\in M$, geschrieben $x
  \stackrel{\mathcal G}{\sim} y$, wenn es ein $g\in\mathcal G$ mit
  $y=gx$ ist.  Eine Äquivalenzklasse $[x]_{\mathcal G}$ unter der
  Relation $\stackrel{\mathcal G}{\sim}$ heißt \acct{Orbit} oder
  \acct{Bahn} des Elements $x\in M$.  Gibt es nur einen Orbit in $M$,
  so heißt die Wirkung von $\mathcal G$ \acct{transitiv}.
\end{definition}

\begin{notice}[Anmerkungen:]
  \begin{itemize}
  \item $\stackrel{\mathcal G}{\sim}$ ist Äquivalenzrelation (ohne Beweis)
  \item Die Menge der Orbits wird $M/\mathcal G$ bezeichnet.
  \end{itemize}
\end{notice}

\begin{example}
  Sei $\mathcal G = \mathrm{SO}(3)$ und $M = \mathbb R^3$.  Die Orbits
  von $\mathrm{SO}(3)$ auf $\mathbb R^3$ sind Kugelschalen. Alle $\bm
  x\in\mathbb R^3$ mit $\lvert\bm x\rvert = r$ liegen im selben Orbit.
  \begin{align*}
    M/\mathcal G
    &\simeq \text{Menge aller Radien} = \mathbb R^3/\mathrm{SO}(3) \\
    &= [0,\infty) \;.
  \end{align*}

  $\ee^{\ii H t/\hbar} \ket{\psi}$ ist der Orbit der Zeitentwicklung.
  \begin{equation*}
    M = \text{Hilbertraum} \;,\quad \mathcal G = \mathrm{U}(n) \;.
  \end{equation*}
\end{example}

\begin{definition}
  \label{def:2.17}
  Eine Teilmenge $N \subset M$ heißt $\mathcal G$-invariant, wenn $gN
  \subset N$ für alle $g\in\mathcal G$.  $\mathcal G$ ist also
  Symmetriegruppe von $N$, vgl.\ Definition~\ref{def:2.9}.
\end{definition}

\begin{theorem}
  \label{thm:2.6}
  Es wirke $\mathcal G$ auf $M$.  Sei $x\in M$ und $\mathcal
  H<\mathcal G$ die Fixpunktgruppe von $x$, d.h.\ $\mathcal H x = x$
  (Def.~\ref{def:2.9}). Dann gilt
  \begin{enumerate}
  \item Die Nebenklasse $g\mathcal H$ besteht aus genau den Elementen,
    welche $x$ in $y=gx$ überführen.
  \item Die Abbildung
    \begin{equation*}
      \mathcal G x \to \mathcal G/\mathcal H
    \end{equation*}
    ist bijektiv.  Ist $\mathcal G$ endlich, so besitzt der Orbit
    genau genau $[\mathcal G:\mathcal H]$ Elemente.
  \end{enumerate}
\end{theorem}

\subsection{Linkstranslation}

\begin{definition}
  \label{def:2.18}
  Sei $k\in\mathcal G$.  Die Abbildung $L(k)$ mit
  \begin{equation*}
    L(k)\colon \mathcal G\to\mathcal G
    \;,\quad g \mapsto k g
  \end{equation*}
  heißt \acct{Linkstranslation}.
\end{definition}

\begin{notice}[Anmerkungen:]
  \begin{itemize}
  \item $L(k)$ ist Automorphismus von $\mathcal G$, aber nicht der
    Gruppe $(G,\cdot)$.
  \item Es gilt $L(k_1,k_2) = L(k_1) L(k_2)$, somit ist
    \begin{equation*}
      L\colon \mathcal G\to\Aut\mathcal G
      \;,\quad
      k \mapsto L(k)
    \end{equation*}
    ein Homomorphismus (sogar Monomorphismus) und eine Gruppenwirkung,
    also
    \begin{equation}
      \label{eq:2.4}
      \mathcal G \text{ ist isomorph zum } \im L < \Aut\mathcal G \;.
    \end{equation}
  \end{itemize}
\end{notice}

\begin{theorem}
  \label{thm:2.7}
  (Satz von Cayley) Jede endliche Gruppe $\mathcal G$ mit
  $\lvert\mathcal G\rvert = n \in \mathbb N$ ist isomorph zu einer
  Untergruppe der symmetrischen Gruppe $S_n$.
\end{theorem}

\begin{proof}
  \begin{itemize}
  \item $\Aut\mathcal G = S_n$ (siehe Beispiele nach Def.~\ref{def:2.7})
  \item Nach \eqref{eq:2.4} ist $\mathcal G \simeq \im L <
    \Aut\mathcal G = S_n$.
  \end{itemize}
\end{proof}

\begin{theorem}
  \label{thm:2.8}
  Es wirke $\mathcal G$ auf $M$, $M$ sei Fixpunktgruppe von $x\in M$
  und es sei $y=gx$, $g\in\mathcal G$.  Dann ist die Fixpunktgruppe
  von $y$ gleich $g \mathcal H g^{-1} = \{ g h g^{-1}, h \in \mathcal
  H \}$ und heißt konjugiert zu $\mathcal H$.
\end{theorem}

\section{Normalteiler}

\begin{definition}
  \label{def:2.19}
  Eine Untergruppe $\mathcal H$ von $\mathcal G$ heißt
  \acct{Normalteiler} oder \acct{invariante Untergruppe} in $\mathcal
  G$, geschrieben $\mathcal H \lhd \mathcal G$, wenn für jedes
  $g\in\mathcal G$ gilt $g \mathcal H = \mathcal H g$ oder $\{ gh, h
  \in \mathcal H \} = \{ hg, h \in \mathcal H \}$ oder für jedes $h
  \in \mathcal H$ und alle $g \in \mathcal G$ gilt $g h g^{-1} \in
  \mathcal H$.
\end{definition}

\begin{notice}[Beachte:]
  Es wird \acct{nicht} $g h g^{-1} = h$ gefordert.
\end{notice}

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../Gruppentheorie.tex"
%%% End:
