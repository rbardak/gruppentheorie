Weiterhin: Wenn der endlich dimensionale Vektorraum $V$ in eine
direkte Summe invarianter Unterraume
\begin{equation}
  \label{eq:4.11}
  V = V^1 \oplus V^2 \oplus V^3 \oplus \cdots \oplus V^n
\end{equation}
derart zerlegt werden kann, dass eine Darstellung von $\mathcal G$ auf
jedem Teilraum irreduzibel ist, so heißt $D$ \acct{vollständig
  reduzierbar} oder \acct{vollreduzibel}.

\begin{notice}[Konsequenz:]
  $\bm N(g) = 0$, $\forall g \in \mathcal G$ in \eqref{eq:4.10}.
\end{notice}

\subsection{Unitäre invariante Unterräume}

Wir betrachten einen unitären Vektorraum $V$ und seinen echten
Unterraum $W$, sowie $V = W \oplus W^\bot$, wobei
\begin{equation}
  \label{eq:4.12}
  W^\bot = \{ \bm v \in V \mid \braket{\bm v|\bm w}= 0 \text{ für alle } w \in W \} \;.
\end{equation}

\begin{theorem}
  \label{thm:4.2}
  Sei $D$ eine reduzible unitäre Darstellung einer Gruppe $\mathcal G$ auf $V$
  und $W$ ein echter invarianter Unterraum von $V$, so ist auch
  $W^\bot$ einer.
\end{theorem}

\begin{proof}
  Sei $\bm u \in W^\bot$, dann ist
  \begin{equation*}
    \braket{D(g) \bm u|\bm w}
    = \braket{\bm u|D^\dagger(g) \bm w}
    = \braket{\bm u|\underbrace{D(g^{-1}) \bm w}_{\in W}}
    = 0 \;,
  \end{equation*}
  also $D(g) \bm u \in W^\bot$, $\forall g \in \mathcal G$.
\end{proof}

\begin{notice}[Folge:]
  Jede endlichedimensionale unitäre Darstellung kann zerlegt werden in
  eine direkte Summe irreduzibler unitärer Darstellungen
  \begin{equation}
    \label{eq:4.13}
    D = D^{(1)} \oplus D^{(2)} \oplus D^{(3)} \oplus \cdots \oplus D^{(S)} \;.
  \end{equation}
  Mit Darstellungsmatrizen in Blockform
  \begin{equation}
    \label{eq:4.14}
    \bm D(g) =
    \begin{pmatrix}
      \bm D^{(1)} & & & 0 \\
      & \bm D^{(2)} \\
      & & \ddots \\
      0 & & & \bm D^{(S)} \\
    \end{pmatrix} \;.
  \end{equation}
\end{notice}

\begin{notice}[Wichtige Konsequenz:]
  Für jede endliche Gruppe oder solche mit invariantem Maß können ihre
  endlichen Darstellungen vollständig reduziert werden, denn nach
  Satz~\ref{thm:4.1} ist jede derartige Darstellung äquivalent zu
  einer unitären.

  Die Matrizen nach \eqref{eq:4.14} sind nicht eindeutig, aber in
  Äquivalenzklassen $D^{(\alpha)}$ einteilbar.  Man schreibt dann
  \begin{equation}
    \label{eq:4.15}
    D = \bigoplus_{\alpha=1}^{k} n_\alpha D^{(\alpha)} \;,\quad n_\alpha \text{: Häufigkeit} \;.
  \end{equation}
\end{notice}

\section{Bedeutende mathematische Grundlagen}

Viele der später folgenden physikalischen Anwendungen beruhen auf
grundlegenden mathematischen Aussagen, die hier vorgestellt werden.

\subsection{Schur-Lemmata}

\begin{theorem}
  \label{thm:4.3}
  Seien $D^{(i)}\colon \mathcal G \to \mathrm{GL}(V^i)$, $i=1,2$ zwei
  irreduzible Darstellungen $\mathcal G$.  Sei $A\colon V^1 \to V^2$
  eine lineare Abbildung mit
  \begin{equation}
    \label{eq:4.16}
    D^{(2)}(g) A = A D^{(1)}(g)
  \end{equation}
  für alle $g\in \mathcal G$, d.h.\ das Diagramm
  \begin{center}
    \begin{tikzcd}
      V^1 \ar[r,"D^{(1)}(g)"] \ar[d,"A"] & V^1 \ar[d,"A"] \\
      V^2 \ar[r,"D^{(2)}(g)"] & V^2 \\
    \end{tikzcd}
  \end{center}
  kommutiert.  Dann gilt
  \begin{enumerate}
  \item $A$ ist ein Isomorphismus oder $A=0$.
  \item Falls $D^{(1)}$ und $D^{(2)}$ nicht äquivalent sind, so folgt $A=0$.
  \item Ist $D=D^{(1)}=D^{(2)}$ und $V=V^1=V^2$ ein komplexer
    Vektorraum, dann gibt es ein $\lambda\in\mathbb C$ mit
    \begin{equation*}
      \bm A = \lambda \mathds 1_V
    \end{equation*}
    (gilt auch für unendliche Gruppen).
  \end{enumerate}
\end{theorem}

\subsection{Orthogonalitätsrelationen}

Mit Hilfe von Satz~\ref{thm:4.3} erhalten wir weitere Relationen, die
von praktischem Nutzen sind.

\begin{theorem}
  \label{thm:4.4}
  Seien $D^{(i)}\colon \mathcal G \to \mathrm{GL}(V^i)$, $i=1,2$
  irreduzible Darstellungen von $\mathcal G$ und $B\colon V^1\to V^2$
  eine beliebige lineare Abbildung.  Sei ferner
  \begin{equation*}
    A = \frac{1}{\lvert \mathcal G \rvert} \sum_{g\in\mathcal G} D^{(2)}(g^{-1}) B D^{(1)}(g)
    \colon V^1 \to V^2
  \end{equation*}
  \begin{center}
    \begin{tikzcd}
      V^1 \ar[d,"D^{(1)}(g)"] \ar[r,"A"] & V^2 \ar[d,"D^{(2)}(g^{-1})"] \\
      V^1 \ar[r,"B"] & V^2 \\
    \end{tikzcd}
  \end{center}
  Dann gilt:
  \begin{enumerate}
  \item Falls $D^{(1)}$ und $D^{(2)}$ nicht äquivalent sind, ist
    $A=0$.
  \item Falls $D=D^{(1)}=D^{(2)}$ und $V=V^1=V^2$, so ist
    \begin{equation}
      \label{eq:4.17}
      \bm A = \lambda \mathds 1_V \quad\text{mit}\quad \lambda = \frac{1}{\gr D} \tr B\;.
    \end{equation}
  \end{enumerate}
\end{theorem}

\begin{theorem}
  \label{thm:4.5}
  Falls $D^{(1)}$ und $D^{(2)}$ nicht äquivalent sind, so gilt für
  alle $i,j,k,\ell$
  \begin{equation}
    \label{eq:4.18}
    \frac{1}{\lvert \mathcal G \rvert} \sum_{g\in\mathcal G} D^{(2)}_{ik}(g^{-1}) D^{(1)}_{\ell j}(g) = 0\;.
  \end{equation}
\end{theorem}

\begin{theorem}
  \label{thm:4.6}
  Falls $D=D^{(1)}=D^{(2)}$ und $V=V^1=V^2$, so gilt
  \begin{equation}
    \label{eq:4.19}
    \frac{\gr D}{\lvert \mathcal G \rvert} \sum_{g\in\mathcal G}
    D_{ik}(g^{-1}) D_{\ell j}(g) = \delta_{ij}\delta_{k\ell}\;.
  \end{equation}
\end{theorem}

\begin{notice}[Zusammenfassung:]
  \begin{equation}
    \label{eq:4.20}
    \boxed{
      \frac{1}{\lvert \mathcal G \rvert} \sum_{g\in\mathcal G} D^{(\alpha)}_{ik}(g^{-1}) D^{(\beta)}_{\ell j}(g)
      = \frac{1}{\gr D^{(\alpha)}} \delta_{\alpha\beta} \delta_{ij}\delta_{k\ell}
    }
  \end{equation}
  oder speziell für unitäre Matrizen
  \begin{equation}
    \label{eq:4.21}
    \boxed{
      \frac{1}{\lvert \mathcal G \rvert} \sum_{g\in\mathcal G} D^{(\alpha)}_{ki}(g)^* D^{(\beta)}_{\ell j}(g)
      = \frac{1}{\gr D^{(\alpha)}} \delta_{\alpha\beta} \delta_{ij}\delta_{k\ell}
    }
  \end{equation}
  Dabei heißt $\alpha=\beta$ identisch, $\alpha\neq\beta$ nicht
  äquivalent.
\end{notice}

Sind $D^{(\alpha)}$ und $D^{(\beta)}$ äquivalent, aber nicht identisch, so gilt mit
\begin{equation}
  \label{eq:4.22}
  \begin{gathered}
    \bm D^{(\alpha)} = \bm S^{-1} \bm D^{(\beta)} \bm S \\
    \frac{1}{\lvert \mathcal G \rvert} \sum_{g\in\mathcal G} D^{(\alpha)}_{ki}(g)^* D^{(\beta)}_{\ell j}(g)
      = \frac{1}{\gr D^{(\alpha)}} S_{\ell k} S_{ij}^{-1}
  \end{gathered}
\end{equation}

\subsection{Satz von Burnside}

Für die Grade der äquivalenten irreduziblen Darstellungen von $\alpha$
gilt:
\begin{equation}
  \label{eq:4.23}
  (\gr D^{(1)})^2 + (\gr D^{(2)})^2 + \cdots (\gr D^{(r)})^2 = \lvert \mathcal G \rvert \;.
\end{equation}
Dies ist ein wichtiges Werkzeug um zu bestimmen wie viele irreduzible
Darstellungen es gibt.

\section{Charaktere}

Die Darstellungsmatrizen werden oft nicht benötigt, weil einfachere
Relationen reichen (Charaktere).

\subsection{Definition}

\begin{definition}
  \label{def:4.5}
  Sei $\bm A \in \mathbb C^{n\times n}\colon \tr \bm A =
  \sum_{i=1}^{n} A_{ii}$ heißt \acct{Spur}.
\end{definition}

\begin{notice}[Erinnerung:]
  \begin{itemize}
  \item linear:
    \begin{equation}
      \label{eq:4.24}
      \text{linear:}\quad \tr(\lambda_1 \bm A_1 + \lambda_2 \bm A_2) = \lambda_1 \tr bm A_1 + \lambda_2 \tr \bm A_2 \\
    \end{equation}
    \begin{subequations}
    \item Basisunabhängigkeit
      \begin{equation}
        \label{eq:4.25b}
        \tr\bm A = \tr(\bm T \bm A \bm T^{-1})
      \end{equation}
    \item Mit Eigenwerten $\lambda_i$ von $\bm A$:
      \begin{equation}
        \label{eq:4.25c}
        \tr\bm A = \sum_{i=1}^{n} \lambda_i
      \end{equation}
    \end{subequations}
  \end{itemize}
\end{notice}

Die Spur ist basisunabhängig, also für einen Endomorphismus $\bm A$
\begin{equation}
  \label{eq:4.26}
  \tr A = \tr \bm A
\end{equation}
Es gilt:
\begin{equation}
  \label{eq:4.27}
  \tr(AB) = \tr(BA)
\end{equation}

\begin{definition}
  Sei $D\colon \mathcal G\to\mathrm{GL}(V)$ eine Darstellung von
  $\mathcal G$.  Die Abbildung
  \begin{align*}
    \chi_D \colon \mathcal G &\to \mathbb C \\
    g &\mapsto \tr(D(g))
  \end{align*}
  heißt Charakter von $D$. Umgekehrt heißt eine Abbildung
  \begin{equation*}
    \chi\colon \mathcal G \to \mathbb C
  \end{equation*}
  Charakter von $D$, wenn eine Darstellung von $D$ existiert mit $\chi
  = \chi_D$.  Der Charakter einer irreduziblen Darstellung heißt
  irreduzibel.
\end{definition}

\subsection{Folgerungen}

Äquivalente Darstellungen besitzen denselben Charakter.  Bei direkten Summen
\begin{equation}
  \label{eq:4.28}
  \chi_{D^{(1)} \oplus D^{(2)}} = \chi_{D^{(1)}} + \chi_{D^{(2)}}\;.
\end{equation}
Kommutativität
\begin{equation}
  \label{eq:4.29}
  \chi_D(gh) = \chi_D(hg) \;.
\end{equation}
Bei unitären Darstellungen gilt
\begin{equation}
  \label{eq:4.30}
  \chi(g^{-1}) = \chi^*(g) \;,\quad \forall g\in\mathcal G
\end{equation}
Es gilt
\begin{equation}
  \label{eq:4.31}
  \chi_D(hgh^{-1}) = \chi_D(g) \;.
\end{equation}
Alle Elemente in einer Konjugationsklasse haben denselben Charakter.
Der Charakter des neutralen Elements ist die Dimension der Darstellung
\begin{equation*}
  \chi_D(e) = \gr D \;.
\end{equation*}


%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "deutsch"
%%% TeX-master: "../Gruppentheorie.tex"
%%% End:
