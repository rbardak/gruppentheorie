\begin{theorem}
  \label{thm:2.14}
  Eine Untergruppe ist ein Normalteiler dann und nur dann, wenn sie
  aus vollständigen Konjugationsklassen besteht.
\end{theorem}

\section{Neue Gruppen aus alten}

\subsection{Direktes Produkt}

\begin{definition}
  \label{def:2.23}
  Das \acct{direkte Produkt} $\mathcal G \times \mathcal G'$ ist
  diejenige Gruppe, welche aus allen geordneten Paaren $(g,g')$ mit $g
  \in \mathcal G$ und $g' \in \mathcal G'$ besteht und das
  Gruppenprodukt
  \begin{equation}
    \label{eq:2.6}
    (g_1, g_1') \cdot (g_2, g_2') = (g_1 g_2, g_1' g_2')
  \end{equation}
  besitzt.
\end{definition}

\begin{notice}[Folgerungen:]
  \begin{itemize}
  \item Einselement $(e,e')$
  \item Inverses $(g,g')^{-1} = (g^{-1}, g'^{-1})$
  \item $\{ e \} \times \mathcal G' = \{ (e,g') \mid g' \in \mathcal G'
    \} \cong \mathcal G'$
  \item $(g,e') \cdot (e,g') = (e,g') \cdot (g,e')$, also kommutieren die Elemente der Untergruppe $\{ e \} \times \mathcal G'$ mit den Elementen der Untergruppe $\mathcal G \times \{ e' \}$.
  \item Jedes Element aus $\mathcal G \times \mathcal G'$ kann
    eindeutig als Produkt eines Elements aus $\mathcal G \times \{ e'
    \}$ und eines aus $\{ e \} \times \mathcal G'$ geschrieben werden.
  \end{itemize}
\end{notice}

\begin{theorem}
  \label{thm:2.15}
  Eine Gruppe $\mathcal G$ ist das direkte Produkt ihrer Untergruppen
  $\mathcal H$ und $\mathcal K$, geschrieben $\mathcal G = \mathcal H
  \times \mathcal K$, wenn gilt
  \begin{enumerate}
  \item\label{itm:2.15-i} $h k = k h$ für alle $h\in\mathcal H,
    k\in\mathcal K$.
  \item\label{itm:2.15-ii} jedes $g\in\mathcal G$ kann eindeutig
    geschrieben werden als $g = h k$ mit $h\in\mathcal H, k\in\mathcal
    K$.
  \end{enumerate}
  $\mathcal H$ und $\mathcal K$ heißen direkte Faktoren von $\mathcal
  G$.
\end{theorem}

\begin{notice}[Folgerungen:]
  \begin{itemize}
  \item $\mathcal H \cap \mathcal K = \{ e \}$, denn wenn $g \in
    \mathcal H \cap \mathcal K$, dann ist $g = g e = e g$.  Die
    Reihenfolge muss jedoch nach Satz~\ref{thm:2.15}-\ref{itm:2.15-i}
    eindeutig sein, somit ist $g = e$.
  \item $\mathcal H$ und $\mathcal K$ sind Normalteiler in $\mathcal
    G$: $\mathcal H \lhd \mathcal G$, $\mathcal K \lhd \mathcal G$.
  \item Die Klassenprodukte sind $\mathcal K_{ij} = \mathcal H_i
    \times \mathcal K_j$, wobei $\mathcal H_i$ ist Klasse in $\mathcal
    K$, $\mathcal K_j$ ist Klasse in $\mathcal H$.
  \item Die Faktorgruppen $\mathcal G/\mathcal H$ und $\mathcal
    G/\mathcal K$ sind isomorph zu $\mathcal K$ bzw.\ $\mathcal H$:
    \begin{align*}
      \mathcal G/\mathcal H &\cong \mathcal K \\
      \mathcal G/\mathcal K &\cong \mathcal H
    \end{align*}
  \end{itemize}
\end{notice}

\subsection{Verallgemeinerung}

Definition~\ref{def:2.23} und Satz~\ref{thm:2.15} können auf direkte
Produkte der Form
\begin{equation*}
  \mathcal G = \mathcal G_1 \times \mathcal G_2 \times \cdots \times \mathcal G_n
\end{equation*}
von $n$ Gruppen erweitert werden.

\subsection{Semidirekte Gruppen}

Nicht immer lässt sich die Forderung~\ref{itm:2.15-i} aus
Satz~\ref{thm:2.15} erfüllen.  Dafür führt man eine Verallgemeinerung
ein.  Ein wichtiges Beispiel dafür ist, dass Drehungen und
Translationen nicht vertauschen.

\begin{definition}
  \label{def:2.24}
  Seien $\mathcal H$ und $\mathcal K$ Gruppen und $\mathcal H$ wirke
  auf $\mathcal K$ (Def.~\ref{def:2.8}), d.h.\ es gibt einen
  Homomorphismus
  \begin{align*}
    Q\colon \mathcal H &\to \Aut\mathcal K \\
    h &\mapsto Q_h \\
    Q_h\colon \mathcal K &\to \mathcal K \\
    k &\mapsto Q_h(k)
  \end{align*}
  Das \acct[semidirektes Produkt]{semidirekte Produkt} $\mathcal H
  \wedge \mathcal K$ ist die Menge aller geordneten Paare $(h,k)$ mit
  dem Gruppenprodukt
  \begin{equation}
    \label{eq:2.7}
    (h_1,k_1) \cdot (h_2,k_2) = (h_1 h_2, k_1 Q_{h_1}(k_2)) \;.
  \end{equation}
\end{definition}

\begin{notice}[Folgerungen:]
  \begin{itemize}
  \item Das Einselement ist $(e,e')$.
  \item Etwas Rechnung zeigt, dass das Assoziativgesetz gilt.
  \item Das Inverse ist $(h,k)^{-1} = (h^{-1}, Q_{h^{-1}}(k^{-1}))$.
  \end{itemize}
  Damit ist $\mathcal H \wedge \mathcal K$ eine Gruppe.
  \begin{itemize}
  \item Falls $Q_h = \id = \text{Identität}$, dann ist $\mathcal H
    \wedge \mathcal K = \mathcal H \times \mathcal K$.
  \item Es sind $\mathcal H \wedge \{ e' \} \cong \mathcal H$ und $\{
    e \} \cong \mathcal K \cong \mathcal K$ Untergruppen von $\mathcal
    G$ mit $\mathcal H \cap \mathcal K = \{ e \}$.
  \item Die Elemente von $\mathcal G$ bestehen aus den geordneten
    Paaren $g = (h,k)$.  Mit $\tilde h \cong (h,e)$ und $\tilde k =
    (e,k)$
    \begin{equation}
      \label{eq:2.8}
      g = h k = \tilde k \tilde h
    \end{equation}
    Beachte die nicht intuitive Reihenfolge in \eqref{eq:2.8}.  Diese
    ist entscheidend, denn es wäre
    \begin{align*}
      \tilde h \tilde k &= (h,e) (e,k) = (he,e Q_h(k)) \\
      &= (h,Q_h(k)) \neq (h,k)
    \end{align*}
  \item $\mathcal K$ ist Normalteiler von $\mathcal G$
    \begin{equation*}
      \mathcal G/\mathcal K \cong \mathcal H \;.
    \end{equation*}
    Man kann also aus $\mathcal H \wedge \mathcal K$ eine neue Gruppe
    bilden.  Wann lässt sich eine Gruppe $\mathcal G$ als semidirektes
    Produkt schreiben?
  \end{itemize}
\end{notice}

\begin{theorem}
  \label{thm:2.16}
  Seien $\mathcal H$ und $\mathcal K$ Untergruppen von $\mathcal G$,
  $\mathcal K \lhd \mathcal G$, $\mathcal H \cap \mathcal K = \{ e \}$
  und $g \in \mathcal G$ werde eindeutig dargestellt als Produkt $g =
  k h$, $h \in \mathcal H$, $k \in \mathcal K$.  Dann kann $\mathcal
  G$ geschrieben werden als semidirektes Produkt
  \begin{equation*}
    \mathcal G = \mathcal H \wedge \mathcal K \;.
  \end{equation*}
  $\mathcal H$ wirkt auf $\mathcal K$ über
  \begin{equation}
    \label{eq:2.9}
    Q_h(k) = h k h^{-1} \;.
  \end{equation}
\end{theorem}

\begin{notice}[Folgerungen:] (ohne Beweis)
  Ähnlich wie beim direkten Produkt gilt
  \begin{equation*}
    \mathcal G / \mathcal K \cong \mathcal H \;.
  \end{equation*}
  $\mathcal H$ und $\mathcal K$ können nicht vertauscht werden.  Es
  ist nicht $\mathcal H \lhd \mathcal G$ gefordert.
\end{notice}

\subsection{Beispiel}
\label{sec:2.8.4}

Die Euklidische Gruppe $\mathrm{E}(3)$ besteht aus
\begin{itemize}
\item Drehspiegelungen im affinen Raum
  \begin{align*}
    \mathbb R^3 &\to \mathrm{O}(3) \\
    x &\mapsto \bm R x \;,\quad \bm R^\tp \bm R = 1
    \;,\quad \det\bm R = \pm 1
  \end{align*}
\item Translationen $\mathrm{T}(3)$ abelsche Gruppe
  \begin{equation*}
    \bm x \mapsto \bm x + \bm t \;,\quad \bm t\in\mathbb R^3 \;.
  \end{equation*}
\end{itemize}
Die Euklidische Gruppe ist nun das semidirekte Produkt
\begin{equation}
  \label{eq:2.10}
  \mathrm{O}(3) \wedge \mathrm{T}(3) = \{ \{ \bm R,\bm t \}, \bm R \in \mathrm{O}(3), \bm t \in \mathrm{T}(3) \}
\end{equation}
mit dem Gruppenprodukt
\begin{equation}
  \label{eq:2.11}
  \{ \bm R_1,\bm t_1 \} \{ \bm R_2,\bm t_2 \} =
  \{ \bm R_1 \bm R_2, \bm t_1 + \bm R_2 \bm t_2 \}
\end{equation}
d.h.\ die Wirkung von $\mathrm O(3)$ auf $mathrm T(3)$ ist die Wirkung
auf $\mathbb R^3$.

Die Wirkung auf ein Element $\bm x \in \mathbb R^3$ ist
\begin{equation*}
  \{ \bm R,\bm t \} \bm x = \bm R \bm x + \bm t
\end{equation*}
Für ein Produkt gilt
\begin{align*}
  \{ \bm R_1,\bm t_1 \} \{ \bm R_2, \bm t_2 \}
  &= \{ \bm R_1,\bm t_1 \} ( \bm R_2 \bm x + \bm t_2 ) \\
  &= \bm R_1 \bm R_2 \bm x + \bm R_1 \bm t_2 + \bm t_1 \\
  &= \{ \bm R_1 \bm R_2, \bm t_1 + \bm R_1 \bm t_2 \} \bm x \;.
\end{align*}
Dies bestätigt die sinnvolle Definition in~\eqref{eq:2.11}.

Das Gruppenprodukt lässt sich in einer geeigneten
$4\times4$-Matrixdarstellung einfach als Matrixmultiplikation
realisieren.
\begin{equation*}
  \{ \bm R,\bm t \} = 
  \begin{pmatrix}
    \bm R & \bm t \\
    0 & 1 \\
  \end{pmatrix}
  \;,\quad
  \bm x \to 
  \begin{pmatrix}
    \bm x \\ 1 \\
  \end{pmatrix}
\end{equation*}
denn es ist
\begin{equation*}
  \begin{pmatrix}
    \bm R & \bm t \\
    0 & 1 \\
  \end{pmatrix}
  \begin{pmatrix}
    \bm x \\ 1 \\
  \end{pmatrix}
  =
  \begin{pmatrix}
    \bm R \bm x + \bm t \\ 1 \\
  \end{pmatrix}
\end{equation*}
und
\begin{equation*}
  \begin{pmatrix}
    \bm R_1 & \bm t_1 \\
    0 & 1 \\
  \end{pmatrix}
  \begin{pmatrix}
    \bm R_2 & \bm t_2 \\
    0 & 1 \\
  \end{pmatrix}
  =
  \begin{pmatrix}
    \bm R_1 \bm R_2 & \bm R_1 \bm t_2 + \bm t_1 \\
    0 & 1 \\
  \end{pmatrix} \;.
\end{equation*}
\begin{itemize}
\item $\mathrm{T}(3)$ ist Normalteiler von $\mathrm{E}(3)$.
\item $\mathrm{E}(3)/\mathrm{E}(3) \cong \mathrm{O}(3)$.
\end{itemize}


%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../Gruppentheorie.tex"
%%% End:
