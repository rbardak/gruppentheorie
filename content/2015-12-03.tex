\subsection{Eine physikalische Konsequenz}

Betrachten wir ein Spin-$1/2$-Teilchen, z.B.\ ein Elektron, in
Ortsdarstellung.
\begin{subequations}
\begin{equation}
  \label{eq:3.23a}
  \braket{r|\psi} = \chi_\up(\bm r) \ket{\up} + \chi_\dn(\bm r) \ket{\dn}
\end{equation}
lässt sich darstellen als
\begin{equation}
  \label{eq:3.23b}
  \bm\chi(\bm r) =
  \begin{pmatrix}
    \chi_\up(\bm r) \\
    \chi_\dn(\bm r)
  \end{pmatrix} \;.
\end{equation}
\end{subequations}
Wie ändert sich die Wellenfunktion bei einer Rotation des Raumes?
\begin{equation}
  \label{eq:3.24}
  U(\hat n,\varphi) \bm\chi(\bm r) = \bm\chi(\bm R^{-1}(\hat n,\varphi) \bm r) \;.
\end{equation}
Wir erwarten offensichtlich
\begin{align*}
  \bm\chi^\dagger(\bm R^{-1}\bm r) \bm\chi(\bm R^{-1}\bm r)
  &= \bm\chi^\dagger(\bm r) \bm\chi(\bm r) \\
  &= \bm\chi^\dagger(\bm r) U^\dagger U \bm\chi(\bm r)
\end{align*}
also ist $U$ unitär.

Die Darstellung der Rotationen im Raum ist bekannt.
\begin{equation*}
  \bm R \eqrel{eq:3.12}{=} \ee^{-\ii \hat n \bm L \varphi} \;.
\end{equation*}
Aus der Theorie der Liegruppen (siehe weiter hinten) folgt: Es gibt
genau eine unitäre Darstellung $U$, nämlich die, die Form wie
\eqref{eq:3.12} hat.  Diese kennen wir bereits
\begin{equation*}
  \tag{\ref{eq:3.18}}
  U = \ee^{-\ii \hat n \bm s \varphi}
\end{equation*}
oder mit \eqref{eq:3.24}
\begin{equation}
  \label{eq:3.25}
  \ee^{-\ii \hat n \bm s \varphi} \bm\chi = \chi(\ee^{\ii \hat n \bm L \varphi} \bm r) \;.
\end{equation}
Aus \eqref{eq:3.25} kann man eine Besonderheit des Spins ablesen, bei
einer Drehung um $\varphi = 2\pi$ ergibt sich
\begin{equation*}
  \ee^{-\ii 2\pi \hat n \bm s} \bm\chi \eqrel{eq:3.16}{=} (\mathds 1 \cos\pi - \ii \hat n \bm\sigma \sin\pi) \bm\chi = -\bm\chi \;.
\end{equation*}
Die Spinwellenfunktion ändert ihr Vorzeichen bei einer vollständigen
Drehung.

Anderes Argument: Es erscheint sinnvoll, dass es einen Homomorphismus
gibt, der zwischen $U$ und $\bm R$ vermittelt \eqref{eq:3.21a}.  Es
war entscheidend, dass $\bm s$ und nicht $\bm\sigma$ mit $\bm s =
\bm\sigma/2$ verwendet wurde.  Dies legt den Faktor $1/2$ im Winkel
fest.

\subsection{Mit Spiegelungen: \texorpdfstring{$\mathrm{O}(3)$}{O(3)}}

Zusätzlich zu $\mathrm{SO}(3)$ Spiegelungen mit Matrixdarstellung aus
Abbildung~\ref{fig:8.1}.

\begin{figure}[tb]
  \centering
  \missingfigure{}
  \caption{Spiegelung an einer Ebene beschrieben durch die
    Ebenennormale $\hat n$.}
  \label{fig:8.1}
\end{figure}

\begin{align}
  \notag
  \bm v
  &= \underbrace{\hat n (\hat n \cdot \bm v)}_{\parallel \hat n}
  + \underbrace{\bm v - \hat n (\hat n \cdot \bm v)}_{\bot \hat n} \\
  \label{eq:3.26}
  &= \{ \hat n \otimes \hat n + \mathds 1 - \hat n \otimes \hat n \} \bm v \;.
\end{align}
Spiegelung:
\begin{align}
  \notag
  \bm\sigma(\hat n) \bm v
  &\eqrel{eq:3.26}{=} - \hat n (\hat n \cdot \bm v) + \bm v - \hat n (\hat n \cdot \bm v) \\
  \notag
  &= \bm v - 2 \hat n (\hat n \cdot \bm v) \\
  \label{eq:3.27}
  &= \underbrace{\{ \mathds 1 - 2 \hat n \otimes \hat n \}}_{\bm\sigma(\hat n)} \bm v \;.
\end{align}

\begin{example}
  $\hat n = \hat e_3 = \text{Spiegelung an der $x$-$y$-Ebene}$.
  \begin{align*}
    \bm\sigma(\hat e_3)
    &\eqrel{eq:3.27}{=} \mathds 1 - 2 \hat e_3 \otimes \hat e_3 \\
    &=
    \begin{pmatrix}
      1 \\ & 1 \\ && 1 \\
    \end{pmatrix}
    - 2
    \begin{pmatrix}
      \\ \\ && 1 \\
    \end{pmatrix} \\
    &=
    \begin{pmatrix}
      1 \\ & 1 \\ && -1 \\
    \end{pmatrix}
    &=
    \underbrace{
      \begin{pmatrix}
        -1 \\ & -1 \\ && -1 \\
      \end{pmatrix}
    }_{\bm i}
    \underbrace{
      \begin{pmatrix}
        -1 \\ & -1 \\ && 1 \\
      \end{pmatrix}
    }_{\bm R(\hat e_3,\pi)} \\
    &= \bm i \bm R(\hat e_3,\pi)
  \end{align*}
\end{example}

\section{Symmertien im affinen Raum: \texorpdfstring{$\mathrm{E}(3)$}{E(3)}}

\subsection{Der affine Raum}

Kommen Verschiebungen (Translationen) hinzu, so wird nicht nur die
Richtung des Vektors geändert, sondern auch sein Anknüpfungspunkt.  Um
dies behandeln zu können, führen wir ein:

\begin{definition}
  \label{def:3.1}
  Ein Tripel $(A,\varphi,V)$ heißt ein affiner Raum, wenn gilt:
  \begin{enumerate}
  \item $A$ ist eine nichtleere Menge, $V$ ist ein Vektorraum,
    $\varphi\colon A\times A \to V$ ist eine surjektive Abbildung
    \begin{equation*}
      (P,Q) \stackrel{\varphi}{\mapsto} \varphi(P,Q) = \overrightarrow{PQ}
    \end{equation*}
  \item Zu $P\in A$ und $\bm v\in V$ existiert genau ein $Q\in A$ mit
    $\overrightarrow{PQ} = \bm v$ (Abtragen eines Vektors $\bm v$ vom
    Punkt $P$).
  \item Für alle $P, Q, R \in A$ gilt $\overrightarrow{PQ} +
    \overrightarrow{QR} = \overrightarrow{PR}$ (Dreiecksregel).
  \item Aus $\overrightarrow{PQ} = 0$ folgt $P = Q$.
  \end{enumerate}
  $A$ heißt Punktraum, $V$ heißt Differenzraum.
\end{definition}

Typischerweise im Ortsraum: Sowohl die Punkte als auch die Differenzen
(Vektoren) werden durch dreikomponentige Spaltenvektoren dargestellt,
also $A = \mathbb R^3$, $V = \mathbb R^3$
\begin{equation*}
  \varphi(\underbrace{\bm x,\bm y}_{\in A}) = \underbrace{\bm y - \bm x}_{\in V}
\end{equation*}
Es gelten alle Betrachtungen zu $\mathrm{E}(3)$ aus \ref{sec:2.8.4}.
  
\subsection{Einige Elemente (Symmetrieoperationen) aus
  \texorpdfstring{$\mathrm{E}(3)$}{E(3)}}

\begin{itemize}
\item Rotationen $\{ \bm R(\hat n,\varphi), 0 \}$
\item Translationen $\{ \mathds 1,\bm t \}$
\end{itemize}

Rotationen um eine Achse, die durch $\bm a\neq 0$ geht:
\begin{equation}
  \label{eq:3.28}
  \{ \mathds 1, \bm a \} \{ \bm R, 0 \} \{ \mathds 1, \bm a \}
  \eqrel{eq:2.11}{=} \{ \bm R, \bm a - \bm R \bm a \} \;.
\end{equation}
Besitzt $\{ \bm R, \bm t \}$ immer Fixpunkte?
\begin{equation*}
  \{ \bm R, \bm t \} \bm a = \bm a
  \iff
  \bm R \bm a + \bm t = \bm a
\end{equation*}
oder
\begin{align*}
  \bm t &= \bm a - \bm R \bm a \\
  &\eqrel{eq:3.12}{=} \left[ 2 (\ii\hat n\bm L)^2 \sin^2 \frac\varphi2 + \ii \hat n\bm L \sin \varphi \right] \bm a \\
  &\eqrel{eq:3.11a}{=} 2 \underbrace{\hat n \times \hat n \times \bm a}_{= 0} \sin^2 \frac\varphi2 - \underbrace{(\hat n\times\bm a)}_{\bot\hat n} \sin\varphi
  \quad\bot\hat n \;.
\end{align*}
Es gibt einen Fixpunkt, wenn $\bm t \bot \hat n$, siehe
Abbildung~\ref{fig:8.2}.

\begin{figure}[tb]
  \centering
  \missingfigure{}
  \caption{In der Ebene $\bot \hat n$ betrachtet.}
  \label{fig:8.2}
\end{figure}

Für ein beliebiges $\bm t = \bm t_1 + \bm t_2$ mit $\bm t_1 \parallel
\hat n$ und $\bm t_2 \bot \hat n$:
\begin{equation*}
  \{ \bm R(\hat n,\varphi), \bm t \}
  \eqrel{eq:2.11}{=}
  \underbrace{\{ \mathds 1, \bm t_1 \}}_{\text{Translation}}
  \underbrace{\{ \bm R(\hat n,\varphi), \bm t_2 \}}_{\text{Drehung mit Fixpunkt}}
\end{equation*}
Das Ergebnis ist eine Schraubung (Abb.~\ref{fig:8.3}).

\begin{figure}[tb]
  \centering
  \missingfigure{}
  \caption{Eine Schraubung ist eine Translation mit einer Drehung mit
    Fixpunkt.}
  \label{fig:8.3}
\end{figure}

Spiegelungen an einer Ebene, die durch den Ursprung geht:
\begin{equation*}
  \{ \bm\sigma(\hat n), 0 \} \quad\text{vgl.~\eqref{eq:3.27}}
\end{equation*}
Ebene durch $\bm a \neq 0$
\begin{equation}
  \label{eq:3.29}
  \{ \mathds 1, \bm a \} \{ \bm\sigma(\hat n), 0 \} \{ \mathds 1, \bm a \}^{-1}
  = \{ \bm\sigma(\hat n), \bm a - \bm\sigma(\hat n) \bm a \} \;.
\end{equation}

Spiegelung mit Translation $\bm t$, Fixpunkt:
\begin{equation*}
  \bm a = \bm\sigma(\hat n) \bm a + \bm t
\end{equation*}
oder
\begin{equation*}
  \bm t = \bm a - \bm\sigma(\hat n) \bm a \eqrel{eq:3.27}{=} 2 \hat n (\hat n \cdot \bm a) \quad \parallel \hat n
\end{equation*}
also nur vorhanden, wenn $\bm t \parallel \hat n$
(Abb.~\ref{fig:8.4}).

\begin{figure}[tb]
  \centering
  \missingfigure{}
  \caption{Ist $\bm t \parallel \hat n$, so gibt es eine ganze Fixebene.}
  \label{fig:8.4}
\end{figure}

Mit $\bm t = \bm t_1 + \bm t_2$, $\bm t_1 \parallel \hat n$, $\bm t_2
\bot \hat n$:
\begin{equation*}
  \{ \bm\sigma(\hat n), \bm t \} = \{ \mathds 1, \bm t_2 \} \{ \bm\sigma(\hat n), \bm t_1 \} \;.
\end{equation*}
Spiegelung mit anschließender Translation (Abb.~\ref{fig:8.5}).

\begin{figure}[tb]
  \centering
  \missingfigure{}
  \caption{Eine Spiegelung mit anschließender Translation heißt
    Gleitspiegelung.}
  \label{fig:8.5}
\end{figure}

Rotationsinversion:
\begin{equation}
  \label{eq:3.30}
  \{ \bm S(\hat n,\varphi), 0 \} = \{ \bm R(\hat n,\varphi) i, 0 \}
\end{equation}
durch $\bm a \neq 0$:
\begin{equation*}
  \{ \mathds 1,\bm a \} \{ \bm S(\hat n,\varphi), 0 \} \{ \mathds 1,\bm a \}^{-1}
  = \{ \bm S, \bm a - \bm S \bm a \} \;.
\end{equation*}
Fixpunkt, wenn
\begin{equation}
  \label{eq:3.31}\relax
  [\mathds 1 - \bm S(\hat n,\varphi)] \bm a = \bm t \;.
\end{equation}
Das Gleichungssystem \eqref{eq:3.31} ist lösbar
\begin{equation}
  \label{eq:3.32}
  \det(\mathds 1 - \bm S) =
  \begin{vmatrix}
    1 + \cos\varphi & -\sin\varphi & 0 \\
    \sin\varphi & 1 + \cos\varphi & 0 \\
    0 & 0 & 2 \\
  \end{vmatrix}
  = 4 (1 + \cos\varphi)
  \neq 0 \quad\text{für}\quad \varphi \neq \pi \;.
\end{equation}

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../Gruppentheorie.tex"
%%% End:
