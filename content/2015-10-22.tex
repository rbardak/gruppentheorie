\begin{definition}
  \label{def:2.2}
  Eine Gruppe $\mathcal G$ heißt abelsch oder kommutativ, wenn für
  alle $g,h \in \mathcal G$ gilt
  \begin{equation*}
    g \cdot h = h \cdot g
  \end{equation*}
\end{definition}

\subsection{Beispiele}

\begin{enumerate}
\item \enquote{Generalized-Linear}-Gruppen $\mathrm{GL}(n)$,
  Orthogonale Gruppen $\mathrm{O}(n)$, Spezielle orthogonale Gruppen
  $\mathrm{SO}(n)$, Spzielle unitäre Gruppen $\mathrm{SU}(n)$, Unitäre
  Gruppen $\mathrm{U}(n)$.
\item $(\mathbb Z,+)$, $(\mathbb Q,+)$, $(\mathbb R,+)$, etc.
  Beachte, dass diese Gruppen viele zusätzliche Eigenschaften haben,
  die nicht alle Gruppen haben.
\item $(\mathbb Q \setminus \{ 0 \}, \cdot)$, etc.
\item Die Menge bijektiver Abbildungen
  \begin{equation*}
    \begin{tikzcd}
      f\colon \mathcal M \to \mathcal M, &
      \mathcal M \arrow[r, "g"] \arrow[rr, bend right, "f\circ g"] &
      \mathcal M \arrow[r, "f"] &
      \mathcal M      
    \end{tikzcd}
  \end{equation*}
\item Permutationen
  \begin{equation*}
    \begin{pmatrix}
      1 & 2 & 3 & 4 & 5 \\
      f(1) & f(2) & f(3) & f(4) & f(5) \\
    \end{pmatrix}
  \end{equation*}
\item Abstrakt: Verknüpfung über Gruppentafeln
  \begin{equation*}
    \begin{array}{c@{\qquad}cccc}
      \cdot & e & a & b & c \\
      \midrule
      e & e & a & b & c \\
      a & a & e & c & b \\
      b & b & c & e & a \\
      c & c & b & a & e \\
    \end{array}
  \end{equation*}
  Die Tafel der einfachsten nichttrivialen Gruppe sieht wie folgt aus
  \begin{equation*}
    \begin{array}{c@{\qquad}cc}
      \cdot & e & g \\
      \midrule
      e & e & g \\
      g & g & e \\
    \end{array}
  \end{equation*}
\end{enumerate}

\subsection{Erste Folgerungen}

Die folgenden Eigenschaften lassen sich aus der
Definition~\ref{def:2.1} folgern.
\begin{itemize}
\item Das Linksinverse ist äquivalent zum Rechtsinversen.
\item Das Linkseinselement ist äquivalent zum Rechtseinselement.
\item Zu $g,h \in \mathcal G$ existiert genau ein $x \in \mathcal G$
  mit $g \cdot x = h$ und genau ein $y \in \mathcal G$ mit $y \cdot g
  = h$.
\item Kürzungsregel: $a x = a y \implies x = y$ und $u a = v a
  \implies u = v$.
\item $\forall a,b \in \mathcal G, (a^{-1})^{-1} = a, (a \cdot b)^{-1}
  = b^{-1} a^{-1}$.
\end{itemize}

\subsection{Untergruppen}

\begin{definition}
  \label{def:2.3}
  Eine \acct{Untergruppe} $\mathcal H$ von $\mathcal G$, geschrieben als
  $\mathcal H < \mathcal G$, ist eine nichtleere Teilmenge $\mathcal
  H$ von $\mathcal G$, die bezüglich des Gruppenprodukts abgeschlossen
  ist.  Dabei muss gelten $e \in \mathcal H$ und wenn $h \in \mathcal
  H$, dann mus auch $h^{-1} \in \mathcal H$ sein.
\end{definition}

\begin{theorem}
  \label{thm:2.1}
  Das \acct{Untergruppenkriterium}.  Eine nichtleere Teilmenge
  $\mathcal H$ ist Untergruppe von $\mathcal G$, genau dann, wenn
  \begin{equation*}
    g,h \in \mathcal H \implies g\cdot h^{-1} \in \mathcal H
  \end{equation*}
  für alle $g,h \in \mathcal H$.  Die Bezeichnung ist $\mathcal H <
  \mathcal G$.
\end{theorem}

\begin{example}
  \begin{enumerate}
  \item Die beiden trivialen (oder auch uneigentlichen) Untergruppen
    $(\{e\},\cdot) < \mathcal G$ und $\mathcal G<\mathcal G$.  Alle
    anderen heißen \enquote{echt} oder \enquote{eigentlich}.
  \item $(\mathbb Z,+) < (\mathbb Q,+) < (\mathbb R,+) < (\mathbb C,+)$.
  \item $\mathrm{SU}(n) < \mathrm{U}(n) < \mathrm{GL}(n)$.
  \item $\mathrm{SO}(2) < \mathrm{SO}(3)$.
  \end{enumerate}
\end{example}

\section{Morphismen}

Morphismen sind Abbildungen zwischen Mengen gleichen Struktur, welche
die Struktur bewahren.  Wir betrachten das hier an der Struktur der
Gruppe.

\subsection{Gruppenhomomorphismus}

\subsubsection{Definitionen}

\begin{definition}
  \label{def:2.4}
  Seien $(\mathcal G,\cdot)$ und $(\mathcal G',\circ)$ Gruppen.  Eine
  Abbildung
  \begin{equation*}
    f\colon \mathcal G\to\mathcal G'
  \end{equation*}
  heißt \acct{Homomorphismus}, wenn für alle $g_1,g_2 \in \mathcal G$
  gilt
  \begin{equation*}
    f(g_1\cdot g_2) = f(g_1) \circ f(g_2) \;.
  \end{equation*}
  Schreibweise bei abelschen Gruppen:
  \begin{equation*}
    f(g_1+g_2) = f(g_1) + f(g_2) \;.
  \end{equation*}
\end{definition}

\begin{definition}
  \label{def:2.5}
  (\acct{Kern} und \acct{Bild} eines Homomorphismus).  Der Kern eines
  Homomorphismus ost das Urbild des neutralen Elements $e' \in
  \mathcal G'$
  \begin{equation*}
    \ker f = \{ g \in \mathcal G \mid f(g) = e' \} = f^{-1}(e') \; .
  \end{equation*}
  Das Bild von $f$ besteht aus den Elementen von $\mathcal G'$, die
  ein Urbild in $\mathcal G$ haben
  \begin{equation*}
    \im f = f(\mathcal G) = \{ f(g) \mid g\in \mathcal G \} \;.
  \end{equation*}
\end{definition}

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \node[MidnightBlue!40,dot,inner sep=1cm,label={[MidnightBlue!40]above:$\mathcal G$}] (G) at (-2,0) {};
    \node[MidnightBlue!60,dot,inner sep=1cm,label={[MidnightBlue!60]above:$\mathcal G'$}] (G') at (+2,0) {};
    \node[MidnightBlue!40,dot,inner sep=.6cm,label={[MidnightBlue!40]above:$\im f$}] (im) at (G') {};
    \node[DarkOrange3,dot,inner sep=.2cm,label={[DarkOrange3]left:$\ker f$}] (ker) at (G) {};
    \node[MidnightBlue,dot,label={[MidnightBlue]above:$e$}] (e) at (ker) {};
    \node[DarkOrange3,dot,label={[DarkOrange3]right:$e'$}] (e') at (G') {};

    \draw[shorten >=2pt,shorten <=2pt,->] (G) to[bend left] node[above] {$f$} (im);
    \draw[shorten >=2pt,shorten <=2pt,->] (ker) to[bend right] node[below] {$f$} (e');
  \end{tikzpicture}
  \caption{Zu Definition~\ref{def:2.5}.}
  \label{fig:2-1}
\end{figure}

\subsubsection{Folgerungen (o.B.)}

\begin{itemize}
\item $f(e) = e'$, d.h.\ $e \in \ker f$
\item $f(g^{-1}) = (f(g))^{-1} \;\forall g\in\mathcal G$
\item $\ker f < \mathcal G$
\item $\im f < \mathcal G'$
\item Seien $r\colon \mathcal G \to \mathcal G'$ und $s\colon\mathcal
  G' \to \mathcal G''$ Homomorphismen, dann ist auch ihre Verkettung
  $s\circ r \colon\mathcal G\to\mathcal G''$ ein Homomorphismus.
  \begin{tikzcd}
    \mathcal G \arrow[r, "r"] \arrow[rr, bend right, "s\circ r"] &
    \mathcal G' \arrow[r, "s"] &
    \mathcal G''
  \end{tikzcd}
\item $\mathcal G\to\mathcal G' \text{ injektiv} \iff \ker f = \{ e \}$
\end{itemize}

\subsection{Weitere Definitionen}

\begin{definition}
  \label{def:2.6}
  Ein Homomorphismus $f\colon\mathcal G\to\mathcal G'$ heißt
  \begin{itemize}
  \item \acct{Epimorphismus}, wenn $f$ surjektiv ist,
  \item \acct{Monomorphismus}, wenn $f$ injektiv ist,
  \item \acct{Isomorphismus}, wenn $f$ bijektiv ist.
  \end{itemize}
  Weitere Bezeichnungen:
  \begin{itemize}
  \item Ein Homomorphismus $f\colon\mathcal G\to\mathcal G$ heißt
    \acct{Endomorphismus}.
  \item Ein Isomorphismus $f\colon\mathcal G\to\mathcal G$ heißt
    \acct{Automorphismus}.
  \end{itemize}
  Sind zwei Gruppen isomorph (es existiert ein Isomorphismus $\mathcal
  G\to\mathcal G'$), so schreibt man
  \begin{equation*}
    \mathcal G \simeq \mathcal G' \;.
  \end{equation*}
\end{definition}

\subsection{Beispiele}

\begin{itemize}
\item $\mathbb Z_2$ ist die Gruppe mit den ganzen Zahlen $0$ und $1$
  mit der Addition modulo $2$ als Verknüpfung.  Die Gruppe $\mathcal G
  = (\{1,-1\},\cdot)$ ist isomorph zu $\mathbb Z_2$ mit dem
  Isomorphismus
  \begin{align*}
    f\colon\mathcal G & \to\mathbb Z_2 \\
    1 & \mapsto 0 \\
    -1 & \mapsto 1
  \end{align*}
\item Die reellen Zahlen lassen sich mit der Exponentialfunktion auf
  die positiven reellen Zahlen abbilden.
  \begin{align*}
    f\colon (\mathbb R,+) &\to (\mathbb R^+,\cdot) \\
    x &\mapsto \ee^x \\
    x+y &\mapsto \ee^{x+y} = \ee^x \cdot \ee^{y}
  \end{align*}
\end{itemize}

\subsection{Verallgemeinerung}

Wir betrachten Morphismen allgemein, daher:

\begin{definition}
  \label{def:2.7}
  Seien $M$ und $M'$ mathematische Objekte mit derselben
  mathematischen Struktur, z.B.\ eine Menge, Gruppe, Körper,
  Vektorraum, affiner Raum, topologischer Raum oder eine
  differenzierbare Mannigfaltigkeit.

  Eine Abbildung $f\colon \mathcal M \to \mathcal M'$, welche die
  Struktur bewahrt heißt Morphismus (oder Abbildung, Homomorphismus,
  stetig glatte Abbildung).

  % 29.10.2015
  Ist der Morphismus bijektiv, so existiert die Umkehrabbildung
  $f^{-1}\colon \mathcal M'\to\mathcal M$ und man nennt ihn
  Isomorphismus.

  Ist $\mathcal M'=\mathcal M$, so bildet die Menge der Isomorphismen
  die \acct{Automorphismengruppe} auf $\mathcal M$.
\end{definition}

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../Gruppentheorie.tex"
%%% End:
