Normalteiler sind somit besondere Untergruppe, die erlauben, dass auch
die Faktormenge Gruppenstruktur hat.  Dies wird ausgedrückt in

\begin{theorem}
  \label{thm:2.9}
  Sei $\mathcal G$ eine Gruppe, $\mathcal H < \mathcal G$ Untergruppe,
  und $\mathcal G/\mathcal H = \{ g\mathcal H \mid g \in \mathcal G
  \}$ die Faktormenge.  Wenn $\mathcal H$ Normalteiler in $\mathcal G$
  ist, so kann man $\mathcal G/\mathcal H$ durch die Vorschrift
  \begin{gather*}
    g,k \in \mathcal G \; , \quad
    [g],[k] \in \mathcal G/\mathcal H \\
    [g] \cdot [k] \defineeq [g\cdot k]
    \quad\text{oder}\quad
    g \mathcal H \cdot k \mathcal H = g k \mathcal H
  \end{gather*}
  zu einer Gruppe, der sogenannten \acct{Faktorgruppe} machen.
\end{theorem}

Wir kennen bereits einen besonderen Normalteiler.

\begin{theorem}
  \label{thm:2.10}
  Ist $f\colon\mathcal G \to \mathcal G'$ ein Homomorphismus, so ist
  $\ker f$ Normalteiler in $\mathcal G$.
\end{theorem}

\begin{proof}
  In den Übungen wurde gezeigt, dass $\ker f < \mathcal G$.  Sei $g
  \in \mathcal G$ und $h \in \ker f$, d.h.\ $f(h) = e'$.  Dann gehört
  aber auch $g h g^{-1}$ zum Kern, denn
  \begin{equation*}
    f(g h g^{-1}) = f(g) f(h) f(g^{-1}) = f(g) e' f(g)^{-1} = e' \;.
  \end{equation*}
\end{proof}

Umgekehrt findet man:

\begin{theorem}
  \label{thm:2.11}
  Sei $\mathcal H \lhd \mathcal G$: Die Projektion (oder kanonische
  Abbildung)
  \begin{align*}
    \pi\colon \mathcal G &\to \mathcal G/\mathcal H \\
    g &\mapsto g \mathcal H
  \end{align*}
  ist ein Epimorphismus mit $\ker \pi = \mathcal H$.
\end{theorem}

Die Kenntnis über den Epimorphismus $\pi$ erlaubt die Einführung eines
immer existierenden Monomorphismus.

\begin{theorem}
  \label{thm:2.12}
  (Homomorphiesatz) Zu jedem Homomorphismus $f\colon \mathcal G \to
  \mathcal G'$ gibt es einen Monomorphismus
  \begin{gather*}
    \varphi\colon \mathcal G/\ker f \to \mathcal G' \\
    \text{mit}\quad f = \varphi \circ \pi
  \end{gather*}
  Zur Veranschaulichung:
  \begin{equation*}
    \begin{tikzcd}
      \mathcal G \arrow[rd, "\pi"] \arrow[rr, "f"] & & \mathcal G' \\
      & \mathcal G/\ker f \arrow[ru, "\varphi"] & \\
    \end{tikzcd}
  \end{equation*}
  $\pi$ ist Epimorphismus (surjektiv) nach Satz~\ref{thm:2.11},
  $\mathcal G/\ker f$ ist Normalteiler nach Satz~\ref{thm:2.10} und
  $\varphi$ ist ein Monomorphismus (injektiv) nach
  Satz~\ref{thm:2.12}.
\end{theorem}

\begin{definition}
  \label{def:2.20}
  Eine Gruppe heißt \acct{einfach}, wenn sie keinen eigentlichen
  (d.h.\ nichttrivialen) Normalteiler besitzt.  Sie heißt
  \acct{halbeinfach}, wenn sie keine eigentlichen abelschen
  Normalteiler hat.
\end{definition}

\section{Konjugationsklassen}

In Satz~\ref{thm:2.8} haben wir konjugierte Untergruppen kennen
gelernt.  Was passiert bei der Konjugation eines einzelnen
Gruppenelements?

\subsection{Konjugation eines Elements}

\begin{definition}
  \label{def:2.21}
  Es wirke $\mathcal G$ auf sich selbst über
  \begin{subequations}
    \begin{gather}
      \label{eq:2.5a}
      \begin{aligned}
        \Phi\colon \mathcal G &\to \Aut \mathcal G \\
        k &\mapsto \Phi_k
      \end{aligned} \\
      \label{eq:2.5b}
      \begin{aligned}
        \Phi_k\colon \mathcal G &\to \mathcal G \\
        g &\mapsto k g k^{-1}
      \end{aligned}
    \end{gather}
  \end{subequations}
\end{definition}

\subsubsection{Aussagen (o.B.) und Beziehungen dazu}

\begin{itemize}
\item $\Phi_k$ ist Automorphismus für jedes $k \in \mathcal G$.
\item $\im \Phi$ heißt \acct{Gruppe der inneren Automorphismen}.
\item $\ker \Phi = \{ k \in \mathcal G, k g k^{-1} = g \text{ für alle
  } g \in \mathcal G \}$ heißt \acct{Zentrum} von $\mathcal G$,
  geschrieben $\mathrm{C}(\mathcal G)$ und ist abelscher Normalteiler
  von $\mathcal G$.  Es folgt bei abelschen Gruppen
  $\mathrm{C}(\mathcal G) = \mathcal G$.
\item Die Fixpunktgruppe eines Elements $g \in \mathcal G$ heißt
  \acct{Zentralisator} von $\mathcal G$:
  \begin{equation*}
    \mathrm{C}_{\mathcal G}(g) = \{ k \in \mathcal G \mid k g k^{-1} = g \}
  \end{equation*}
\item Die Fixpunktgruppe einer Untergruppe von $\mathcal G$, $\mathcal
  H < \mathcal G$, heißt \acct{Normalisator} $\mathrm{N}_{\mathcal
    G}(\mathcal H)$.
\item Der Orbit eines Elements $g \in \mathcal G$:
  \begin{equation*}
    \Phi_{\mathcal G} g = \{ k^{-1} g k , k \in \mathcal G \}
  \end{equation*}
  heißt \acct{Konjugationsklasse} von $g$.  In einer anderen
  Sichtweise:

  \enquote{$g$ ist konjugiert zu $h$} meint $g \sim h \iff$ es gibt
  ein $k\in\mathcal G$ mit $h = k g k^{-1} = \Phi_k g$ ist
  Äquivalenzrelation und heißt Konjugationsklasse $[g]$.
\end{itemize}

\subsubsection{Folgerungen}

\begin{itemize}
\item $[e] = \{ e \}$ bildet eine eigene Klasse und heißt
  \acct{selbstkonjugiert}.
\item Für eine abelsche Gruppe gilt, dass alle Elemente
  selbstkonjugiert sind.
\item $g$ ist selbstkonjugiert $\iff [g] = \{ g \} \iff
  \mathrm{C}_{\mathcal G}(g) = \mathcal G$.
\item Bei endlichen Gruppen gilt: Die Zahl $r(g)$ der Elemente in
  $[g]$ ist gleich
  \begin{equation*}
    r(g) = [\mathcal G : \mathrm{C}_{\mathcal G}(g)] =
    \frac{\lvert\mathcal G\rvert}{\lvert\mathrm{C}_{\mathcal
        G}(g)\rvert} \;.
  \end{equation*}
\item
  \begin{itemalign}
    &\sum_{[g]} r(g) = \lvert\mathcal G\rvert \\
    &\sum_{[g]} \frac{1}{\lvert\mathrm{C}_{\mathcal
        G}(g)\rvert} = 1
  \end{itemalign}
\end{itemize}

\begin{example}
  $C_{3v}$: Es gibt drei Konjugationsklassen
  \begin{align*}
    \mathcal K_e &= [e] & r(e) &= 1 \\
    \mathcal K_3 &= [c_3] \{ c_3,c_3^2 \}  & r(c_3) &= 2 \\
    \mathcal K_\sigma &= [\sigma_v] = \{ \sigma_v,\sigma_{v'},\sigma_{v''} \} & r(\sigma_v) &= 3
  \end{align*}
\end{example}

\subsection{Das Klassenprodukt}

\begin{definition}
  \label{def:2.22}
  Seien $\mathcal K_1 = \{ g_1,\ldots,g_r \}$ und $\mathcal K_2 = \{
  h_1,\ldots,h_s \}$ Konjugationsklassen.  Dann ist das
  \acct{Klassenprodukt} definiert als
  \begin{equation*}
    \mathcal K_1 \cdot \mathcal K_2 = \{ g_i \cdot h_i \mid
    i = 1,\ldots,r \;,\; j = 1,\ldots,s \} \;.
  \end{equation*}
\end{definition}

\begin{theorem}
  \label{thm:2.13}
  \begin{enumerate}
  \item Das Klassenprodukt ist kommutativ
    \begin{equation*}
      \mathcal K_1 \cdot \mathcal K_2 = \mathcal K_2 \cdot \mathcal K_1 \;.
    \end{equation*}
  \item Es besteht aus vollständigen Klassen
    \begin{equation*}
      \mathcal K_i \cdot \mathcal K_j = \sum_\ell h_{ij,\ell} k_\ell \;.
    \end{equation*}
    Vereinigung der Mengen, bei der mehrfach auftretende Elemente
    mehrfach berücksichtigt werden.
  \end{enumerate}
\end{theorem}

\begin{example}
  Klassentafel von $C_{3v}$.
  \begin{equation*}
    \begin{array}{c@{\qquad}ccc}
      \toprule
      & \mathcal K_e & \mathcal K_3 & \mathcal K_\sigma \\
      \midrule
      \mathcal K_e & \mathcal K_e & \mathcal K_3 & \mathcal K_\sigma \\
      \mathcal K_3 & & 2 \mathcal K_e + \mathcal K_3 & 2 \mathcal K_\sigma \\
      \mathcal K_\sigma & & & 3 \mathcal K_e + 3 \mathcal K_\sigma \\
      \bottomrule
    \end{array}
  \end{equation*}
\end{example}

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../Gruppentheorie.tex"
%%% End:
