\subsection{Orthogonalitätsrelationen}

Aus \eqref{eq:4.20} folgt $i = k$ und $j = \ell$ und Summation über
alle Elemente

\begin{theorem}
  \label{thm:4.7}
  Seien $\chi^{(\alpha)}$ und $\chi^{(\beta)}$ irreduzible Charaktere,
  dann gilt
  \begin{equation}
    \label{eq:4.32}
    \boxed{
      \frac{1}{\lvert\mathcal G\rvert} \sum_{g\in\mathcal G} \chi^{(\alpha)}(g^{-1}) \chi^{(\beta)}(g)
      = \delta_{\alpha\beta}
    }
  \end{equation}
  bzw.\ für unitäre Darstellungen
  \begin{equation}
    \label{eq:4.33}
    \boxed{
      \frac{1}{\lvert\mathcal G\rvert} \sum_{g\in\mathcal G} \chi^{(\alpha)}(g)^* \chi^{(\beta)}(g)
      = \braket{\chi^{(\alpha)} | \chi^{(\beta)}} = \delta_{\alpha\beta}
    }
  \end{equation}
\end{theorem}

Das hat wichtige Konsequenzen mit praktischem Nutzen.  Wie oft ist
eine irreduzible Darstellung in einer unitären Darstellung enthalten?
Nach \eqref{eq:4.15}
\begin{equation*}
  D = \bigoplus_{\alpha=1}^{k} n_\alpha D^\alpha = n_1 D^{(1)} \oplus n_2 D^{(2)} + \cdots
\end{equation*}
Damit aber auch
\begin{equation}
  \label{eq:4.34}
  \chi = \sum_{\alpha=1}^{k} n_\alpha \chi^{(\alpha)}
\end{equation}
Und somit:
\begin{equation}
  \label{eq:4.35}
  n_\alpha \eqrel{eq:4.33}{=} \braket{\chi|\chi^{(\alpha)}}
\end{equation}
Wann sind zwei unitäre Darstellungen äquivalent?  Wenn ihre Charaktere
übereinstimmen folgt nach \eqref{eq:4.35} dieselbe Zerlegung in
äquivalente Darstellungen, d.h.\ dann sind die Darstellungen
äquivalent.

Wann ist eine Darstellung irreduzibel?
\begin{equation*}
  \braket{\chi|\chi} \eqrel{eq:4.34}[eq:4.32]{=} \sum_\alpha n_\alpha^2
\end{equation*}
also genau dann, wenn $\braket{\chi|\chi} = 1$.

\subsection{Charaktere und Klassen}

Sehr einfach folgt aus \eqref{eq:4.31} und \eqref{eq:4.32}

\begin{theorem}
  \label{thm:4.8}
  $\mathcal G$ besitze $k_g$ Konjugationsklassen mit je
  $m_1,m_2,\ldots,m_{k_g}$ Elementen und $\lvert\mathcal G\rvert =
  \sum_{i=1}^{k_g} m_i$, so lauten die Orthogonalitätsrelationen
  \begin{equation}
    \label{eq:4.36}
    \boxed{
      \frac{1}{\lvert\mathcal G\rvert} \sum_{i=1}^{k_g} \chi^{(\alpha)}(g_i^{-1}) \chi^{(\beta)}(g_i) m_i
      = \delta_{\alpha\beta}
    }
  \end{equation}
  Nur die Repräsentanten werden benötigt.
\end{theorem}

Ohne Beweis muss angegeben werden:

\begin{theorem}
  \label{thm:4.9}
  Die Zahl der irreduziblen Darstellungen einer endlichen Gruppe ist
  gleich der Zahl ihrer Konjugationsklassen.
\end{theorem}

Dies ist ein bemerkenswertes Ergebnis, denn es gibt keine
offensichtliche Verwandtschaft zwischen Konjugationsklassen und
irreduziblen Darstellungen. Der Beweis erfordert die Frobenius-Algebra
und die reguläre Darstellung. Zusammen mit \eqref{eq:4.23} erhält man
schon viel Information über die Darstellungen.

\begin{example}
  $C_{3v}$ besitzt sechs Elemente $\lvert\mathcal G\rvert = 6$ und
  drei Klassen $k_g = 3$.  Mit Satz~\ref{thm:4.9} und \eqref{eq:4.23}
  folgt
  \begin{equation*}
    (\gr D^{(1)})^2 + (\gr D^{(2)})^2 + (\gr D^{(3)}) = 6
  \end{equation*}
  Die einzige Lösung ist
  \begin{itemize}
  \item 1-dim Darstellung $2\times$
  \item 2-dim Darstellung $1\times$
  \end{itemize}
\end{example}

Eine weitere Orthogonalitätsrelation folgt aus \eqref{eq:4.36}.
\begin{align}
  \notag
  \frac{1}{\lvert\mathcal G\rvert} \sum_{i=1}^{k_g}
  \chi^{(\alpha)}(g_i^{-1}) \chi^{(\beta)}(g_i) m_i &\eqrel{eq:4.36}{=} \delta_{\alpha\beta} \\
  \notag
  \sum_{i=1}^{k_g} \chi^{(\alpha)}(g_i^{-1}) \underbrace{
    \sum_\beta \frac{m_i}{\lvert\mathcal G\rvert} \chi^{(\beta)}(g_i) \chi^{(\beta)}(g_j^{-1})
  }_{\delta_{ij}}
  &= \chi^{(\alpha)}(g_j^{-1})
  \intertext{also}
  \label{eq:4.37}
  \sum_\beta \chi^{(\beta)}(g_i) \chi^{(\beta)}(g_j^{-1}) &= \frac{\lvert\mathcal G\rvert}{m_i} \delta_{ij}
\end{align}

\subsection{Die reguläre Rechtsdarstellung}
\label{sec:4.4.5}

\begin{definition}
  \label{def:4.7}
  Die \acct{reguläre Rechtsdarstellung} ist definiert durch die Matrizen
  \begin{subequations}
    \label{eq:4.38}
    \begin{align}
      \label{eq:4.38a}
      R_{ij}(g) = \delta(g_i g g_j^{-1}) \\
      \intertext{mit}
      \label{eq:4.38b}
      \delta(h) =
      \begin{cases*}
        1 & falls $h=e$ \\
        0 & sonst
      \end{cases*}
    \end{align}
  \end{subequations}
\end{definition}

Die reguläre Rechtsdarstellung besteht aus (i.a.\ sehr großen, d.h.\
so groß wie die Gruppentafel) Matrizen.  Ihre Charaktere sind
\begin{equation}
  \label{eq:4.39}
  \chi_R(g) \eqrel{eq:4.38}{=} \sum_{i=1}^{\lvert\mathcal G\rvert} \delta(g_i g g_i^{-1}) =
  \begin{cases*}
    1 & falls $h=e$ \\
    0 & sonst
  \end{cases*}
\end{equation}

Die reguläre Rechtsdarstellung enthält alle irreduziblen Darstellungen
einer Gruppe, es gilt sogar (o.B.):

\begin{theorem}
  \label{thm:4.10}
  Die Vielfachheit der irreduziblen Darstellung $D^{(\alpha)}$ in der
  regulären Rechtsdarstellung $R$ ist $n_\alpha = \gr D^{(\alpha)}$.
\end{theorem}

\begin{notice}[Vorsicht!]
  Die Darstellungen $D^{(\alpha)}$ sind, i.a.\ nicht in Blockform in
  $R$ enthalten.  Sie können nicht einfach aus $R$ abgelesen werden.
  Wie das gelingt folgt später.
\end{notice}

\subsection{Beispiel: Zyklische Gruppen}

\subsubsection{Allgemein}

Zyklische Gruppen haben $\lvert\mathcal G\rvert$ Konjugationsklassen
mit je einem Element.  Also nach \eqref{eq:4.23}:
\begin{equation*}
  \sum_{i=1}^{\lvert\mathcal G\rvert} (\gr D^{(i)})^2 = \lvert\mathcal G\rvert
\end{equation*}
Es gibt $\lvert\mathcal G\rvert$ eindimensionale Darstellungen.

Da alle Darstellungen eindimensional sind, gilt für die Charaktere von
$g$ mit $g^{\lvert\mathcal G\rvert} = e$:
\begin{subequations}
  \label{eq:4.40}
  \begin{equation}
    \label{eq:4.40a}
    \chi^{(\alpha)}(g^{\lvert\mathcal G\rvert}) = \bigl(\chi^{(\alpha)}(g)\bigr)^{\lvert\mathcal G\rvert} = \chi^{(\alpha)}(e) = 1
  \end{equation}
  also
  \begin{equation}
    \label{eq:4.40b}
    \chi^{(\alpha)}(g) = \sqrt[\lvert\mathcal G\rvert]{1}
  \end{equation}
\end{subequations}
eine (!) der $\lvert\mathcal G\rvert$-ten Wurzel von $1$.

\subsubsection{Beispiel: \texorpdfstring{$C_3$}{C3}}

\begin{equation*}
  C_3 = \{c_3,c_3^2,e\} \;,\quad
  \sqrt[3]{1} = 1, \ee^{2\pi\ii/3}, \ee^{4\pi\ii/3}
\end{equation*}
Die Charaktertafel lautet
\begin{equation*}
  \begin{array}{cccc}
    & e & c_3 & c_3^2 \\
    \midrule
    \chi^{(1)} & 1 & 1 & 1 \\
    \chi^{(2)} & 1 & \ee^{2\pi\ii/3} & \ee^{4\pi\ii/3} \\
    \chi^{(3)} & 1 & \ee^{4\pi\ii/3} & \ee^{2\pi\ii/3} \\
  \end{array}
\end{equation*}

\subsubsection{Beispiel: Translationsinvarianz auf einem Gitter}

\begin{figure}[tb]
  \centering
  \missingfigure{}
  \caption{Gitter mit Gitterkonstante $a$.}
  \label{fig:12.1}
\end{figure}

Translationen
\begin{subequations}
  \begin{align}
    \label{eq:4.41a}
    t(a) \;,\quad t(\ell a) &= t(a)^\ell \\
    \label{eq:4.41b}
    t(\ell a) \psi(x) &= \psi(x-\ell a)
  \end{align}
\end{subequations}
Periodische Randbedingungen
\begin{subequations}
  \begin{align}
    \label{eq:4.42a}
    t(N a) \psi(x) &= \psi(x) \\
    \label{eq:4.42b}
    t(N a) &= e
  \end{align}
\end{subequations}
Zyklische Gruppe mit Darstellung = Charakteren
\begin{equation}
  \label{eq:4.43}
  \chi^{(\alpha)}(t(\ell a)) \eqrel{eq:4.40b}{=} \ee^{-2\pi\ii\alpha\ell/N}
\end{equation}
Schreibe
\begin{subequations}
  \begin{align}
    \label{eq:4.44a}
    k = \frac{\alpha}{N} \frac{2\pi}{a} \\
    \label{eq:4.44b}
    \chi^{(\alpha)}(t(\ell a)) \eqrel{eq:4.43}[eq:4.44a]{=} \chi^{(k)}(t(\ell a)) = \ee^{\ii k \ell a}
  \end{align}
\end{subequations}
In der irreduziblen Darstellung $k$ gilt dann für die Basisfunktionen
\begin{equation}
  \label{eq:4.45}
  t(\ell a) \psi_k(x) = \psi_k(x-\ell a) \eqrel{eq:4.44b}{=} \ee^{\ii k \ell a} \psi_k(x)
\end{equation}
Dies ermöglicht das Einführen von
\begin{subequations}
  \begin{equation}
    \label{eq:4.46a}
    \psi_k(a) = \ee^{-\ii k x} M_k(x)
  \end{equation}
  wobei
  \begin{equation}
    \label{eq:4.46b}
    M_k(x-a) \eqrel{eq:4.46a}{=} \ee^{\ii k (x-a)} \psi_k(x-a) \eqrel{eq:4.45}{=} \ee^{\ii k x} \psi_k(x)
    \eqrel{eq:4.46a}{=} M_k(x)
  \end{equation}
  Dies ist das Bloch-Theorem.
\end{subequations}

\section{Reelle Darstellungen}

Oft interessieren in der Physik reelle Darstellungen.  Ob es diese
gibt, entscheidet sich an folgenden Kriterien nach Frobenius und Schur
für irreduzible Darstellungen $D$:
\begin{enumerate}
  \begin{subequations}
  \item $D$ reell bzw.\ kann durch unitäre Transformationen in reelle
    Form gebracht werden, wenn gilt:
    \begin{equation}
      \label{eq:4.47a}
      \frac{1}{\lvert\mathcal G\rvert} \sum_{g\in\mathcal G} \chi(g^2) = +1
    \end{equation}
  \item $D$ ist äquivalent zu seinem konjugiert Komplexen $D^*$, kann
    aber nicht in reelle Form gebracht werden wenn
    \begin{equation}
      \label{eq:4.47b}
      \frac{1}{\lvert\mathcal G\rvert} \sum_{g\in\mathcal G} \chi(g^2) = -1
    \end{equation}
  \item $D$ ist genau dann nicht äquivalent zu $D^*$, wenn
    \begin{equation}
      \label{eq:4.47c}
      \frac{1}{\lvert\mathcal G\rvert} \sum_{g\in\mathcal G} \chi(g^2) = 0
    \end{equation}
  \end{subequations}
\end{enumerate}

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "deutsch"
%%% TeX-master: "../Gruppentheorie.tex"
%%% End:
