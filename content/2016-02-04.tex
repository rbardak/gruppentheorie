Die Sätze~\ref{thm:4.13} und~\ref{thm:4.15} sagen uns, dass es
Projektoren auf die invarianten Teilräume gibt.  Aber wie finden wir
diese?  Dabei hilft

\begin{theorem}
  \label{thm:4.16}
  Sei $D = \bigoplus_{\alpha=1}^{k} n_\alpha D^{(\alpha)}$ in unitäre
  irreduzible Darstellungen zerlegt und $V$ entsprechend in eine Summe
  paarweise orthogonaler Unterräume
  \begin{equation*}
    V = \bigoplus_{\alpha=1}^k \bigoplus_{i=1}^{n_\alpha} V_i^{(\alpha)} \;.
  \end{equation*}
  Sei $\chi^{(\alpha)}$ ein irreduzibler Character von $\mathcal G$.
  Dann ist der lineare Operator
  \begin{equation}
    \label{eq:4.62}
    \mathcal P^{(\alpha)} = \frac{\gr D^{(\alpha)}}{\lvert\mathcal G\rvert}
    \sum_{g\in\mathcal G} \chi^{(\alpha)}(g)^* D(g)
  \end{equation}
  ein Projektor auf den invarianten Teilraum
  \begin{equation*}
    V^{(\alpha)} = \bigoplus_{i=1}^{n_\alpha} V_i^{(\alpha)} \;.
  \end{equation*}
\end{theorem}

\begin{notice}[Konsequenz:]
  Man benötigt nur die Charaktere in Dimensionen der Darstellung (aus
  Tabellen) und erhält den Projektor für die vorhandene Darstellung
  $D$.
\end{notice}

\begin{notice}[Achtung:]
  Die Zerlegung ist nicht eindeutig, wenn $n_\alpha > 1$,
  vgl.~\eqref{eq:4.62}.  Es wird nur auf den Teilraum in dem alle
  äquivalenten Darstellungen enthalten sind projiziert.
\end{notice}

\subsection{Erzeugung einer symmetrieangepassten Basis}

Wie findet man die Basisvektoren für die Blockdiagonaldarstellung?
Dazu definiert man den basisunabhängigen \enquote{starken
  Projektionsoperator} (\emph{Achtung:} Kein Projektor im Sinn von
Satz~\ref{thm:4.12}).
\begin{equation}
  \label{eq:4.63}
  \mathcal P_{\ell k}^{(\alpha)} = \frac{\gr D^{(\alpha)}}{\lvert\mathcal G\rvert}
    \sum_{g\in\mathcal G} D_{\ell k}^{(\alpha)}(g)^* D(g) \;.
\end{equation}

Sei nun im Teilraum $V_i^{(\beta)}$ ein Basisvektor $\bm
e_{ij}^{(\beta)}$ ($j$-ter Basisvektor), dann
\begin{align}
  \notag
  \mathcal P_{\ell k}^{(\alpha)} \bm e_{ij}^{(\beta)}
  &= \frac{\gr D^{(\alpha)}}{\lvert\mathcal G\rvert}
  \sum_{g\in\mathcal G} D_{\ell k}^{(\alpha)}(g)^*
  \sum_{m=1}^{\gr D^{(\beta)}} \underbrace{\bm e_{im}^{(\beta)} D_{mj}^{(\beta)}(g)}_{\eqref{eq:4.3}} \\
  \label{eq:4.64}
  &= \delta_{\alpha\beta} \delta_{k\ell} \bm e_{i\ell}^{(\beta)} \;.
\end{align}
Das heißt $\mathcal P_{\ell k}^{(\alpha)}$ erzeugt aus einem
Basisvektor $\bm e_{ij}^{(\beta)}$ einen Partner-Basisvektor aus
demselben Teilraum, falls $\alpha=\beta$, $k=j$.

Insbesondere für $\ell=k$:
\begin{equation}
  \label{eq:4.65}
  \mathcal P_{kk}^{(\alpha)} \bm e_{ij}^{(\beta)} \eqref{eq:4.64}{=} \delta_{\alpha\beta} \delta_{kj} \bm e_{ik}^{(\beta)} = \delta_{\alpha\beta} \delta_{kj} \bm e_{ij}^{(\beta)}
\end{equation}
Das heißt $\mathcal P_{kk}^{(\alpha)}$ projiziert auf den Raum
$\{e_{jk}^{(\alpha)} \mid 1 \leq j \leq n_\alpha\}$ der Basisvektoren
zur $k$-ten Zeile der $\alpha$-ten irreduziblen Darstellung (mit allen
Vielfachheiten).  Es ist also tatsächlich ein Projektor.

Über folgende Konstruktionsvorschift trennt man nun noch die Teilräume
der Vielfachheiten.
\begin{enumerate}
\item Betrachte den Projektionsoperator $\mathcal
  P_{11}^{(\alpha)}\colon V\to V$.  Das Bild $\im\mathcal
  P_{ii}^{(\alpha)}$ ist der $n_\alpha$"=dimensionale Teilraum der
  Basisvektoren zur ersten Zeile der $\alpha$-ten Darstellung.
\item Wähle eine Orthonormalbasis in diesem Teilraum
  \begin{equation*}
    \{ e_{k1}^{(\alpha)} \mid 1 \leq k \leq n_\alpha \}
  \end{equation*}
  Die Vektoren $\{ e_{kj}^{(\alpha)} \mid 1 \leq j \leq \gr
  D^{(\alpha)} \}$ mit
  \begin{equation*}
    e_{kj}^{(\alpha)} = \mathcal P_{j1}^{(\alpha)} e_{k1}^{(\alpha)}
  \end{equation*}
  bilden eine Orthonormalbasis im Teilraum $V_k^{(\alpha)}$, da
  Basisvektoren zu verschiedenen Zeilen einer unitären Darstellung
  ohnehin orthogonal sind.  Es gilt nun aber auch $V_k^{(\alpha)} \bot
  V_{\ell}^{(\alpha)}$ für $k\neq\ell$, denn
  \begin{equation}
    \label{eq:4.66}
    \braket{e_{kj}^{(\alpha)}|e_{\ell j}^{(\alpha)}} \eqrel{eq:4.64}{=} 
    \braket{\mathcal P_{j1} e_{k1}^{(\alpha)}|\mathcal P_{j1} e_{\ell 1}^{(\alpha)}} =
    \braket{e_{k1}^{(\alpha)}|e_{\ell 1}^{(\alpha)}} = \delta_{\ell k} \;.
  \end{equation}
\end{enumerate}

\section{Symmetrische und antisymmetrische Darstellungen}

\subsection{Äußeres Produkt von Darstellungen}

\begin{theorem}
  \label{thm:4.17}
  Sei $\mathcal G = \mathcal H \times \mathcal K$ ein direktes Produkt
  der Untergruppen $\mathcal H < \mathcal G$ und $\mathcal K <
  \mathcal G$ gemäß Def.~\ref{def:2.23}.  Sei ferner $D\colon \mathcal
  G \to \mathrm{GL}(V)$ eine Darstellung von $\mathcal G$.  Wenn
  $D|_{\mathcal H}\colon\mathcal H \to\mathrm{GL}(V)$ vollständig
  reduzierbar ist und $V$ entsprechend zerlegbar ist
  \begin{equation}
    \label{eq:4.67}
    V = \bigoplus_{\alpha=1}^k \bigoplus_{i=1}^{n_\alpha} V_{i\mathcal H}^{(\alpha)}
      = \bigoplus_{\alpha=1}^k V_{\mathcal H}^{(\alpha)} \;.
  \end{equation}
  so sind die Teilräume $V_{\mathcal H}^{(\alpha)}$ invariant
  bezüglich der Darstellung $D|_{\mathcal K}\colon\mathcal
  K\to\mathrm{GL}(V)$ und folglich auch bezüglich.  (vgl.\ QM:
  Gemeinsame Eigenzustände kommutierender Observablen)
\end{theorem}

\subsection{Anwendung auf die Symmetrisierung von Darstellungen}

Sei $D$ eine Darstellung einer Gruppe $\mathcal G$ über den
Vektorräumen $V = \braket{\bm v_1,\dotsc,\bm v_\alpha}$ und $W =
\braket{\bm w_1,\dotsc,\bm w_\alpha}$.  Auf dem Produktraum $V \otimes
W = \braket{\bm v_i \otimes \bm w_i | i,j=1,\dotsc,\alpha}$ wirkt dann
\begin{enumerate}
\item die Gruppe $\mathcal G$ mit dem direkten Produkt $D \otimes D$.
\item die Permutationsgruppe $S_2 = (\{ e,i \},\cdot)$ mit dem Generator
  \begin{align*}
    S\colon V\otimes W &\to V\otimes W \\
    \bm v_i \otimes \bm w_j &\mapsto \bm v_j\otimes \bm w_i
  \end{align*}
  Dieser kommutiert mit den Elementen von $\mathcal G$:
  \begin{align*}
    S(D\otimes D)(g) \bm v_i\otimes \bm w_i
    &= S(D(g) \bm v_i\otimes D(g) \bm w_i) \\
    &= S\biggl(\sum_{k,\ell} \bm v_k\otimes \bm w_\ell D_{ki}(g) D_{j\ell}(g)\biggr) \\
    &= \sum_{k,\ell} \bm v_\ell\otimes \bm w_k D_{ki}(g) D_{j\ell}(g) \\
    &= D(g) \bm v_j \otimes D(g) \bm w_i \\
    &= (D\otimes D)(g) (\bm v_j \otimes \bm w_i) \\
    &= (D\otimes D)(g) S(\bm v_i \otimes \bm w_j)
  \end{align*}
\item Insgesamt wirkt also das direkte Produkt $\mathcal G \times
  S_2$.  Die invarianten Unterräume von $V\otimes W$ unter $S_2$ sind
  \begin{align*}
    [V\otimes W]_+ &= \braket{\bm v_i\otimes\bm w_j + \bm v_j\otimes\bm w_i} \\
    [V\otimes W]_- &= \braket{\bm v_i\otimes\bm w_j - \bm v_j\otimes\bm w_i}
  \end{align*}
  Sie sind auch invariant unter der Wirkung von $\mathcal G$:
  \begin{align*}
    \MoveEqLeft (D\otimes D)(g) \{ \bm v_i \otimes \bm w_j \pm \bm v_j \otimes \bm w_i \} \\
    &= \bm v_k D_{ki}(g) \otimes \bm w_\ell D_{\ell i}(g) \pm \bm v_k D_{kj}(g) \otimes \bm w_\ell D_{\ell i}(g) \\
    &= \bm v_k \otimes \bm w_\ell \{ D_{ki}(g) D_{\ell i}(g) \pm D_{kj}(g) D_{\ell i}(g) \} \\
    &= \{ \bm v_k \otimes \bm w_\ell \pm \bm v_\ell \otimes \bm w_k \} \frac{1}{2} \{ D_{ki}(g) D_{\ell i}(g) \pm D_{kj}(g) D_{\ell i}(g) \}
  \end{align*}
  vollstädnige Superposition von Elementen aus $[V \otimes W]_\pm$.
\end{enumerate}





%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "deutsch"
%%% TeX-master: "../Gruppentheorie.tex"
%%% End:
